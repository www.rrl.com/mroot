/*
* // +-------------------------------------------------------------------------------------------------
* // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
* // +-------------------------------------------------------------------------------------------------
* // |                             独在异乡为异客         每逢佳节倍思亲
* // +-------------------------------------------------------------------------------------------------
* // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
* // +-------------------------------------------------------------------------------------------------
*/

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.service.system.impl


import org.slf4j.Logger
import org.slf4j.LoggerFactory
import wang.encoding.mroot.common.service.BaseServiceImpl
import org.springframework.stereotype.Service
import wang.encoding.mroot.mapper.system.LoggingEventMapper
import wang.encoding.mroot.model.entity.system.LoggingEvent
import wang.encoding.mroot.service.system.LoggingEventService


/**
 * 后台 logback日志 Service 接口实现类
 *
 * @author ErYang
 */
@Service
class LoggingEventServiceImpl : BaseServiceImpl
<LoggingEventMapper, LoggingEvent>(), LoggingEventService {

    companion object {
        /**
         * logger
         */
        private val logger: Logger = LoggerFactory.getLogger(LoggingEventServiceImpl::class.java)
    }


    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End LoggingEventServiceImpl class

/* End of file LoggingEventServiceImpl.kt */
/* Location: ./src/main/kotlin/wang/encoding/mroot/service/system/impl/LoggingEventServiceImpl.kt */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
