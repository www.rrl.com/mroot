<#-- 表单验证国际化提示信息 -->
<script>

    /**
     * 提示信息
     */
    var AlertMessage = function () {

        var message_alert_confirmation_title = '${I18N("message.alert.confirmation.title")}';

        var message_alert_sweetAlert_confirmButtonText = '${I18N("message.alert.sweetAlert.confirmButtonText")}';

        var message_alert_sweetAlert_cancelButtonText = '${I18N("message.alert.sweetAlert.cancelButtonText")}';

        var message_alert_count_down_timer = '${I18N("message.alert.count.down.timer")}';

        var message_alert_not_info = '${I18N("message.alert.not.info")}';

        var message_alert_network_error = '${I18N("message.alert.network.error")}';

        var message_alert_ajax_ok = '${I18N("message.alert.ajax.ok")}';
        var message_alert_ajax_fail = '${I18N("message.alert.ajax.fail")}';
        var message_alert_ajax_repeat = '${I18N("message.alert.ajax.repeat")}';
        var message_alert_ajax_error = '${I18N("message.alert.ajax.error")}';
        var message_alert_ajax_validation = '${I18N("message.alert.ajax.validation")}';

        var message_alert_pagination_pageSize = '${I18N("message.alert.pagination.pageSize")}';
        var message_alert_pagination_totalRow = '${I18N("message.alert.pagination.totalRow")}';
        var message_alert_pagination_count = '${I18N("message.alert.pagination.count")}';

        return {

            getMessageAlertConfirmationTitle: function () {
                return message_alert_confirmation_title;
            },

            // -------------------------------------------------------------------------------------------------

            getMessageAlertSweetAlertConfirmButtonText: function () {
                return message_alert_sweetAlert_confirmButtonText;
            },

            // -------------------------------------------------------------------------------------------------

            getMessageAlertSweetAlertCancelButtonText: function () {
                return message_alert_sweetAlert_cancelButtonText;
            },

            // -------------------------------------------------------------------------------------------------

            getMessageAlertCountDownTimer: function () {
                return message_alert_count_down_timer;
            },

            // -------------------------------------------------------------------------------------------------

            getMessageAlertNotInfo: function () {
                return message_alert_not_info;
            },

            // -------------------------------------------------------------------------------------------------

            getMessageAlertNetworkError: function () {
                return message_alert_network_error;
            },

            // -------------------------------------------------------------------------------------------------

            getMessageAlertAjaxOk: function () {
                return message_alert_ajax_ok;
            },

            getMessageAlertAjaxFail: function () {
                return message_alert_ajax_fail;
            },
            getMessageAlertAjaxRepeat: function () {
                return message_alert_ajax_repeat;
            },
            getMessageAlertAjaxError: function () {
                return message_alert_ajax_error;
            },
            getMessageAlertAjaxValidation: function () {
                return message_alert_ajax_validation;
            },

            getMessageAlertPaginationPageSize: function () {
                return message_alert_pagination_pageSize;
            },
            getMessageAlertPaginationTotalRow: function () {
                return message_alert_pagination_totalRow;
            },
            getMessageAlertPaginationCount: function () {
                return message_alert_pagination_count;
            }

            // -------------------------------------------------------------------------------------------------

        }
    }();

    // ------------------------------------------------------------------------

</script>
