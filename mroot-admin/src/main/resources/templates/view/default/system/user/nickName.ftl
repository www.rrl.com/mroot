<#-- /* 主要部分 */ -->
<@OVERRIDE name="MAIN_CONTENT">
    <#include "/default/common/pageAlert.ftl">

<#-- 内容开始 -->
<div class="m-portlet m-portlet--mobile">

    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                    ${I18N("message.form.head.title")}
                </h3>
            </div>
        </div>
    </div>

<#-- 表单开始 -->
    <form class="m-form m-form--state m-form--fit m-form--label-align-right" id="nickName_form"
          action="${updateNickName}"
          method="post" autocomplete="off">
        <div class="m-portlet__body">

            <#include "/default/common/formAlert.ftl">

            <@formTokenAndRefererUrl></@formTokenAndRefererUrl>

            <input name="id" type="hidden" value="${user.id}">

            <div class="form-group m-form__group row">
                <label class="col-form-label col-lg-3 col-sm-12">
                    ${I18N("message.form.id")}
                </label>
                <div class="col-lg-4 col-md-9 col-sm-12">
                    <span class="m-form__control-static">${user.id}</span>
                </div>
            </div>

            <div class="form-group m-form__group row">
                <label class="col-form-label col-lg-3 col-sm-12">
                    ${I18N("message.system.user.form.username")}
                </label>
                <div class="col-lg-4 col-md-9 col-sm-12">
                    <span class="m-form__control-static">${user.username}</span>
                </div>
            </div>

            <div class="m-form__seperator m-form__seperator--dashed m-form__seperator--space"></div>


            <div class="form-group m-form__group row">
                <label class="col-form-label col-lg-3 col-sm-12">
                    ${I18N("message.system.user.form.nickName")}&nbsp;*
                </label>
                <div class="col-lg-4 col-md-9 col-sm-12">
                    <input type="text" class="form-control m-input" name="nickName"
                           placeholder="${I18N("message.system.user.form.nickName.text")}"
                           value="${user.nickName}"
                    >
                    <span class="m-form__help">${I18N("message.system.user.form.nickName.text.help")}</span>
                </div>
            </div>

            <div class="form-group m-form__group row">
                <label class="col-form-label col-lg-3 col-sm-12">
                    ${I18N("message.system.user.form.password")}&nbsp;*
                </label>
                <div class="col-lg-4 col-md-9 col-sm-12">
                    <input type="password" class="form-control m-input" name="password"
                           placeholder="${I18N("message.system.user.form.password.text")}"
                           value=""
                    >
                    <span class="m-form__help">${I18N("message.system.user.form.password.text.help")}</span>
                </div>
            </div>


        </div>

    <#-- 提交按钮 -->
    <@formOperate></@formOperate>

    </form>
<#-- 表单结束 -->

</div>
<#-- 内容结束 -->

</@OVERRIDE>
<@OVERRIDE name="PAGE_MESSAGE">
    <#include "/default/system/user/userJs.ftl">
</@OVERRIDE>
<#include "/default/scriptPlugin/form.ftl">
<@OVERRIDE name="CUSTOM_SCRIPT">
<script>
    jQuery(document).ready(function () {
        // 顶部导航高亮
        Tool.highlight_top_nav('${navIndex}');
        FormValidation.formValidationNickname();
    });
</script>
<#-- /* /.页面级别script结束 */ -->
</@OVERRIDE>

<@EXTENDS name="/default/common/base.ftl"/>
