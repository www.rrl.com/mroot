<@OVERRIDE name="MAIN_CONTENT">

<div class="m-portlet m-portlet--mobile">

    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                    ${I18N("message.form.head.title")}
                </h3>
            </div>
        </div>
    </div>

    <form class="m-form m-form--state m-form--fit m-form--label-align-right">
        <div class="m-portlet__body">

            <div class="form-group m-form__group row">
                <label class="col-form-label col-lg-3 col-sm-12">
                    ${I18N("message.form.id")}
                </label>
                <div class="col-lg-4 col-md-9 col-sm-12">
                    <span class="m-form__control-static">${scheduleJob.id}</span>
                </div>
            </div>


            <div class="form-group m-form__group row">
                <label class="col-form-label col-lg-3 col-sm-12">
                    ${I18N("message.form.title")}
                </label>
                <div class="col-lg-4 col-md-9 col-sm-12">
                    <span class="m-form__control-static"><@defaultStr scheduleJob.title></@defaultStr></span>
                </div>
            </div>

            <div class="form-group m-form__group row">
                <label class="col-form-label col-lg-3 col-sm-12">
                    ${I18N("message.form.name")}
                </label>
                <div class="col-lg-4 col-md-9 col-sm-12">
                    <span class="m-form__control-static"><@defaultStr scheduleJob.name></@defaultStr></span>
                </div>
            </div>

            <div class="form-group m-form__group row">
                <label class="col-form-label col-lg-3 col-sm-12">
                    ${I18N("message.system.scheduleJob.form.cronExpression")}
                </label>
                <div class="col-lg-4 col-md-9 col-sm-12">
                    <span class="m-form__control-static"><@defaultStr scheduleJob.cronExpression></@defaultStr></span>
                </div>
            </div>

            <div class="form-group m-form__group row">
                <label class="col-form-label col-lg-3 col-sm-12">
                    ${I18N("message.system.scheduleJob.form.beanName")}
                </label>
                <div class="col-lg-4 col-md-9 col-sm-12">
                    <span class="m-form__control-static"><@defaultStr scheduleJob.beanName></@defaultStr></span>
                </div>
            </div>

            <div class="form-group m-form__group row">
                <label class="col-form-label col-lg-3 col-sm-12">
                    ${I18N("message.system.scheduleJob.form.methodName")}
                </label>
                <div class="col-lg-4 col-md-9 col-sm-12">
                    <span class="m-form__control-static"><@defaultStr scheduleJob.methodName></@defaultStr></span>
                </div>
            </div>

            <div class="form-group m-form__group row">
                <label class="col-form-label col-lg-3 col-sm-12">
                    ${I18N("message.form.status")}
                </label>
                <div class="col-lg-4 col-md-9 col-sm-12">
                    <span class="m-form__control-static"><@statusStrTable  scheduleJob.status></@statusStrTable></span>
                </div>
            </div>


            <div class="m-form__seperator m-form__seperator--dashed m-form__seperator--space"></div>


            <div class="form-group m-form__group row">
                <label class="col-form-label col-lg-3 col-sm-12">
                    ${I18N("message.system.scheduleJob.form.params")}
                </label>
                <div class="col-lg-4 col-md-9 col-sm-12">
                    <span class="m-form__control-static"><@defaultStr scheduleJob.params></@defaultStr></span>
                </div>
            </div>

            <div class="form-group m-form__group row">
                <label class="col-form-label col-lg-3 col-sm-12">
                    ${I18N("message.form.sort")}
                </label>
                <div class="col-lg-4 col-md-9 col-sm-12">
                    <span class="m-form__control-static"><@defaultStr scheduleJob.sort></@defaultStr></span>
                </div>
            </div>

            <div class="form-group m-form__group row">
                <label class="col-form-label col-lg-3 col-sm-12">
                    ${I18N("message.form.gmtCreate")}
                </label>
                <div class="col-lg-4 col-md-9 col-sm-12">
                    <span class="m-form__control-static"><@dateFormat  scheduleJob.gmtCreate></@dateFormat></span>
                </div>
            </div>

            <div class="form-group m-form__group row">
                <label class="col-form-label col-lg-3 col-sm-12">
                    ${I18N("message.form.gmtCreateIp")}
                </label>
                <div class="col-lg-4 col-md-9 col-sm-12">
                    <span class="m-form__control-static"><@defaultStr scheduleJob.gmtCreateIp></@defaultStr></span>
                </div>
            </div>

            <div class="form-group m-form__group row">
                <label class="col-form-label col-lg-3 col-sm-12">
                    ${I18N("message.form.gmtModified")}
                </label>
                <div class="col-lg-4 col-md-9 col-sm-12">
                    <span class="m-form__control-static"><@dateFormat  scheduleJob.gmtModified></@dateFormat></span>
                </div>
            </div>


            <div class="form-group m-form__group row">
                <label class="col-form-label col-lg-3 col-sm-12">
                    ${I18N("message.form.remark")}
                </label>
                <div class="col-lg-4 col-md-9 col-sm-12">
                    <span class="m-form__control-static"><@defaultStr scheduleJob.remark></@defaultStr></span>
                </div>
            </div>

        </div>

    <@viewFormOperate></@viewFormOperate>

    </form>

</div>

</@OVERRIDE>
<#include "/default/scriptPlugin/form.ftl">
<@OVERRIDE name="CUSTOM_SCRIPT">
<script>
    jQuery(document).ready(function () {
        // 顶部导航高亮
        Tool.highlight_top_nav('${navIndex}');
    });
</script>
</@OVERRIDE>

<@EXTENDS name="/default/common/base.ftl"/>
