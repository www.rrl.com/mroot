/*
* // +-------------------------------------------------------------------------------------------------
* // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
* // +-------------------------------------------------------------------------------------------------
* // |                             独在异乡为异客         每逢佳节倍思亲
* // +-------------------------------------------------------------------------------------------------
* // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
* // +-------------------------------------------------------------------------------------------------
*/

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.admin.common.freemarker.modelex

import freemarker.template.TemplateMethodModelEx
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import wang.encoding.mroot.model.entity.system.Rule
import wang.encoding.mroot.service.system.RuleService
import java.math.BigInteger


/**
 * Rule  freemarker 方法类
 *
 * @author ErYang
 */
@Component
class RuleMethodModelEx
@Autowired constructor(private var ruleService: RuleService) : TemplateMethodModelEx {


    /**
     * logger
     */
    private val logger: Logger = LoggerFactory.getLogger(RuleMethodModelEx::class.java)


    override fun exec(list: List<*>): Rule? {
        var rule: Rule? = null
        val id: BigInteger? = list[0].toString().toBigIntegerOrNull()
        if (null != id) {
            rule = ruleService.getById(id)
        }
        return if (null != rule) {
            rule
        } else {
            rule = ruleService.getById(1.toBigInteger())
            rule
        }
    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End RuleMethodModelEx class

/* End of file RuleMethodModelEx.kt */
/* Location: ./src/main/kotlin/wang/encoding/mroot/admin/common/freemarker/modelex/RuleMethodModelEx.kt */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
