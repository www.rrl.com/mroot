/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.admin.common.config


import org.springframework.cache.annotation.EnableCaching
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.scheduling.annotation.EnableAsync
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor

import java.util.concurrent.Executor
import java.util.concurrent.ThreadPoolExecutor


/**
 * 自定义线程池
 *
 * @author ErYang
 */
@Configuration
@EnableCaching
@EnableAsync
class AdminTaskExecutePoolConfiguration {


    companion object {
        /**
         * 线程池维护线程的最少数量
         */
        private const val CORE_POOL_SIZE = 5
        /**
         * 线程池维护线程的最大数量
         */
        private const val MAX_POOL_SIZE = 20
        /**
         * 缓存队列
         */
        private const val QUEUE_CAPACITY = 3
        /**
         * 允许的空闲时间
         */
        private const val KEEP_ALIVE = 60
    }

    // -------------------------------------------------------------------------------------------------

    @Bean
    fun adminTaskExecutePool(): Executor {
        val executor = ThreadPoolTaskExecutor()
        executor.corePoolSize = CORE_POOL_SIZE
        executor.maxPoolSize = MAX_POOL_SIZE
        executor.setQueueCapacity(QUEUE_CAPACITY)
        executor.threadNamePrefix = "AdminTaskExecutePool-"
        // rejection-policy：当 pool 已经达到 max size 的时候 如何处理新任务
        // CALLER_RUNS：不在新线程中执行任务 而是由调用者所在的线程来执行
        executor.setRejectedExecutionHandler(ThreadPoolExecutor.CallerRunsPolicy())
        // 对拒绝 task 的处理策略
        executor.keepAliveSeconds = KEEP_ALIVE
        executor.initialize()
        return executor
    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End AdminTaskExecutePoolConfiguration class

/* End of file AdminTaskExecutePoolConfiguration.kt */
/* Location: ./src/main/kotlin/wang/encoding/mroot/admin/common/config/AdminTaskExecutePoolConfiguration.kt */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
