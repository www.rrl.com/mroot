/*
* // +-------------------------------------------------------------------------------------------------
* // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
* // +-------------------------------------------------------------------------------------------------
* // |                             独在异乡为异客         每逢佳节倍思亲
* // +-------------------------------------------------------------------------------------------------
* // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
* // +-------------------------------------------------------------------------------------------------
*/

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.admin.controller.system.scheduleJob


import com.alibaba.fastjson.JSONObject
import com.baomidou.mybatisplus.plugins.Page
import org.apache.shiro.authz.annotation.RequiresPermissions
import org.quartz.CronExpression
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.NoSuchBeanDefinitionException
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.util.ReflectionUtils
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.ResponseBody
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.servlet.ModelAndView
import org.springframework.web.servlet.mvc.support.RedirectAttributes
import wang.encoding.mroot.admin.common.controller.BaseAdminController
import wang.encoding.mroot.admin.common.event.ScheduleJobOperationEvent
import wang.encoding.mroot.common.annotation.FormToken
import wang.encoding.mroot.common.annotation.RequestLogAnnotation
import wang.encoding.mroot.common.business.ResultData
import wang.encoding.mroot.common.constant.RequestLogConstant
import wang.encoding.mroot.common.exception.ControllerException
import wang.encoding.mroot.model.entity.system.ScheduleJob
import wang.encoding.mroot.model.enums.ScheduleJobOperationTypeEnum
import wang.encoding.mroot.model.enums.StatusEnum
import wang.encoding.mroot.service.system.ScheduleJobService
import java.math.BigInteger
import javax.servlet.http.HttpServletRequest


/**
 * 后台 定时任务 控制器
 *
 *@author ErYang
 */
@RestController
@RequestMapping(value = ["/scheduleJob"])
class ScheduleJobController : BaseAdminController() {


    @Autowired
    private lateinit var scheduleJobService: ScheduleJobService


    companion object {

        /**
         * logger
         */
        private val logger: Logger = LoggerFactory.getLogger(ScheduleJobController::class.java)

        /**
         * 模块
         */
        private const val MODULE_NAME: String = "/scheduleJob"
        /**
         * 视图目录
         */
        private const val VIEW_PATH: String = "/system/scheduleJob"
        /**
         * 对象名称
         */
        private const val VIEW_MODEL_NAME: String = "scheduleJob"
        /**
         * 首页
         */
        private const val INDEX: String = "/index"
        private const val INDEX_URL: String = MODULE_NAME + INDEX
        private const val INDEX_VIEW: String = VIEW_PATH + INDEX
        /**
         * 新增
         */
        private const val ADD: String = "/add"
        private const val ADD_URL: String = MODULE_NAME + ADD
        private const val ADD_VIEW: String = VIEW_PATH + ADD
        /**
         * 保存
         */
        private const val SAVE: String = "/save"
        private const val SAVE_URL: String = MODULE_NAME + SAVE
        /**
         * 修改
         */
        private const val EDIT: String = "/edit"
        private const val EDIT_URL: String = MODULE_NAME + EDIT
        private const val EDIT_VIEW: String = VIEW_PATH + EDIT
        /**
         * 更新
         */
        private const val UPDATE: String = "/update"
        private const val UPDATE_URL: String = MODULE_NAME + UPDATE
        /**
         * 查看
         */
        private const val VIEW: String = "/view"
        private const val VIEW_URL: String = MODULE_NAME + VIEW
        private const val VIEW_VIEW: String = VIEW_PATH + VIEW
        /**
         * 删除
         */
        private const val DELETE: String = "/delete"
        private const val DELETE_URL: String = MODULE_NAME + DELETE
        /**
         * 运行
         */
        private const val RUN: String = "/run"
        private const val RUN_URL: String = MODULE_NAME + RUN
        /**
         * 暂停
         */
        private const val PAUSE: String = "/pause"
        private const val PAUSE_URL: String = MODULE_NAME + PAUSE
        /**
         * 恢复
         */
        private const val RESUME: String = "/resume"
        private const val RESUME_URL: String = MODULE_NAME + RESUME

    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 列表页面
     *
     * @param request HttpServletRequest
     * @return ModelAndView
     */
    @RequiresPermissions(value = [INDEX_URL])
    @RequestMapping(INDEX)
    @RequestLogAnnotation(module = RequestLogConstant.ADMIN_MODULE,
            title = RequestLogConstant.ADMIN_MODULE_SYSTEM_SCHEDULE_JOB_INDEX)
    @Throws(ControllerException::class)
    fun index(request: HttpServletRequest): ModelAndView {
        val modelAndView = ModelAndView(super.initView(INDEX_VIEW))
        super.initViewTitleAndModelUrl(INDEX_URL, MODULE_NAME, request)

        val scheduleJob = ScheduleJob()
        val title: String? = request.getParameter("title")
        if (null != title && title.isNotBlank()) {
            scheduleJob.title = title
            modelAndView.addObject("title", title)
        }
        scheduleJob.status = StatusEnum.NORMAL.key
        val page: Page<ScheduleJob> = scheduleJobService.list2page(super.initPage(request), scheduleJob, ScheduleJob.ID, false)!!
        modelAndView.addObject(VIEW_PAGE_NAME, page)
        return modelAndView
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 添加页面
     *
     * @param request HttpServletRequest
     * @return ModelAndView
     */
    @RequiresPermissions(value = [ADD_URL])
    @RequestMapping(ADD)
    @RequestLogAnnotation(module = RequestLogConstant.ADMIN_MODULE,
            title = RequestLogConstant.ADMIN_MODULE_SYSTEM_SCHEDULE_JOB_ADD)
    @FormToken(init = true)
    @Throws(ControllerException::class)
    fun add(request: HttpServletRequest): ModelAndView {
        val modelAndView = ModelAndView(super.initView(ADD_VIEW))
        super.initViewTitleAndModelUrl(ADD_URL, MODULE_NAME, request)

        // 最大排序值
        val maxSort: Int = scheduleJobService.getMax2Sort() + 1
        modelAndView.addObject("maxSort", maxSort)

        // 设置上个请求地址
        super.initRefererUrl(request)
        return modelAndView
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 处理保存
     *
     * @param request HttpServletRequest
     * @param scheduleJob ScheduleJob
     * @return Any
     */
    @RequiresPermissions(value = [SAVE_URL])
    @RequestMapping(SAVE)
    @RequestLogAnnotation(module = RequestLogConstant.ADMIN_MODULE,
            title = RequestLogConstant.ADMIN_MODULE_SYSTEM_SCHEDULE_JOB_SAVE)
    @FormToken(remove = true)
    @Throws(ControllerException::class)
    fun save(request: HttpServletRequest, scheduleJob: ScheduleJob): Any {

        if (httpRequestUtil.isAjaxRequest(request)) {
            val failResult: ResultData = ResultData.fail()

            // 创建 ScheduleJob 对象
            val saveScheduleJob: ScheduleJob = this.initAddData(request, scheduleJob)

            // Hibernate Validation  验证数据
            val validationResult: String? = scheduleJobService.validationScheduleJob(saveScheduleJob)
            if (null != validationResult && validationResult.isNotBlank()) {
                return super.initErrorHibernateValidationJSONObject(failResult, validationResult)
            }

            // 验证数据唯一性
            val flag: Boolean = this.validationAddData(saveScheduleJob, failResult)
            if (!flag) {
                return super.initErrorValidationJSONObject(failResult)
            }

            try {
                @Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
                val bean: Any? = applicationContext.getBean(saveScheduleJob.beanName)
                if (null != bean) {
                    try {
                        @Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
                        ReflectionUtils.findMethod(bean.javaClass, saveScheduleJob.methodName)
                                ?: return super.initErrorJSONObject(failResult, "message.system.scheduleJob.methodName.error")
                    } catch (e: NoSuchMethodException) {
                        return super.initErrorJSONObject(failResult, "message.system.scheduleJob.methodName.error")
                    }
                }
            } catch (e: NoSuchBeanDefinitionException) {
                return super.initErrorJSONObject(failResult, "message.system.scheduleJob.beanName.error")
            }

            // 检查 cron 表达式
            val flagCron: Boolean = CronExpression.isValidExpression(saveScheduleJob.cronExpression)
            if (!flagCron) {
                return super.initErrorJSONObject(failResult, "message.system.scheduleJob.cronExpression.error")
            }
            // 新增用户 id
            val id: BigInteger? = scheduleJobService.saveBackId(saveScheduleJob)
            if (null != id && BigInteger.ZERO < id) {
                val scheduleJobNew: ScheduleJob? = scheduleJobService.getById(id)!!
                // 异步添加定时任务
                val scheduleJobOperationEvent = ScheduleJobOperationEvent(scheduleJobNew!!,
                        ScheduleJobOperationTypeEnum.ADD)
                super.applicationContext.publishEvent(scheduleJobOperationEvent)

            }
            return super.initSaveJSONObject(id)
        } else {
            return super.initErrorRedirectUrl()
        }
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 编辑页面
     *
     * @param id String
     * @param request HttpServletRequest
     * @param redirectAttributes RedirectAttributes
     * @return ModelAndView
     */
    @RequiresPermissions(value = [EDIT_URL])
    @RequestMapping("$EDIT/{id}")
    @RequestLogAnnotation(module = RequestLogConstant.ADMIN_MODULE,
            title = RequestLogConstant.ADMIN_MODULE_SYSTEM_SCHEDULE_JOB_EDIT)
    @FormToken(init = true)
    @Throws(ControllerException::class)
    fun edit(@PathVariable(ID_NAME) id: String, request: HttpServletRequest,
             redirectAttributes: RedirectAttributes): ModelAndView {
        val modelAndView = ModelAndView(super.initView(EDIT_VIEW))


        super.initViewTitleAndModelUrl(EDIT_URL, MODULE_NAME, request)
        val idValue: BigInteger? = super.getId(id)
        if (null == idValue || BigInteger.ZERO > idValue) {
            return initValidationResult2Get(modelAndView, redirectAttributes, INDEX_URL)
        }
        // 数据真实性
        val scheduleJob: ScheduleJob? = scheduleJobService.getById(idValue)
                ?: return initValidationResult2Get(modelAndView, redirectAttributes, INDEX_URL)
        if (StatusEnum.DELETE.key == scheduleJob!!.status) {
            return initValidationResult2Get(modelAndView, redirectAttributes, INDEX_URL)
        }
        modelAndView.addObject(VIEW_MODEL_NAME, scheduleJob)
        // 设置上个请求地址
        super.initRefererUrl(request)
        return modelAndView
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 处理更新
     *
     * @param request HttpServletRequest
     * @param scheduleJob ScheduleJob
     * @return Any
     */
    @RequiresPermissions(value = [UPDATE_URL])
    @RequestMapping(UPDATE)
    @RequestLogAnnotation(module = RequestLogConstant.ADMIN_MODULE,
            title = RequestLogConstant.ADMIN_MODULE_SYSTEM_SCHEDULE_JOB_UPDATE)
    @FormToken(remove = true)
    @Throws(ControllerException::class)
    fun update(request: HttpServletRequest, scheduleJob: ScheduleJob): Any {
        if (httpRequestUtil.isAjaxRequest(request)) {
            val failResult: ResultData = ResultData.fail()
            // 验证数据
            val idValue: BigInteger? = super.getId(request)
            if (null == idValue || BigInteger.ZERO > idValue) {
                return super.initErrorCheckJSONObject(failResult)
            }
            // 数据真实性
            val scheduleJobBefore: ScheduleJob = scheduleJobService.getById(idValue)
                    ?: return super.initErrorCheckJSONObject(failResult)
            if (StatusEnum.DELETE.key == scheduleJobBefore.status) {
                return super.initErrorCheckJSONObject(failResult)
            }

            // 创建 ScheduleJob 对象
            var editScheduleJob: ScheduleJob = ScheduleJob.copy2New(scheduleJob)
            editScheduleJob.id = scheduleJobBefore.id
            val statusStr: String? = super.getStatusStr(request)
            when {
                statusStr.equals(configProperties.bootstrapSwitchEnabled) ->
                    editScheduleJob.status = StatusEnum.NORMAL.key
                null == statusStr -> editScheduleJob.status = StatusEnum.DISABLE.key
                else -> editScheduleJob.status = StatusEnum.DISABLE.key
            }
            editScheduleJob = this.initEditData(editScheduleJob)

            // Hibernate Validation 验证数据
            val validationResult: String? = scheduleJobService.validationScheduleJob(editScheduleJob)
            if (null != validationResult && validationResult.isNotBlank()) {
                return super.initErrorHibernateValidationJSONObject(failResult, validationResult)
            }

            // 验证数据唯一性
            val flag: Boolean = this.validationEditData(scheduleJob,
                    scheduleJobBefore, failResult)
            if (!flag) {
                return super.initErrorValidationJSONObject(failResult)
            }

            try {
                @Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
                val bean: Any? = applicationContext.getBean(editScheduleJob.beanName)
                if (null != bean) {
                    try {
                        @Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
                        ReflectionUtils.findMethod(bean.javaClass,
                                editScheduleJob.methodName)
                                ?: return super.initErrorJSONObject(failResult,
                                        "message.system.scheduleJob.methodName.error")
                    } catch (e: NoSuchMethodException) {
                        return super.initErrorJSONObject(failResult,
                                "message.system.scheduleJob.methodName.error")
                    }
                }
            } catch (e: NoSuchBeanDefinitionException) {
                return super.initErrorJSONObject(failResult, "message.system.scheduleJob.beanName.error")
            }
            // 检查 cron 表达式
            val flagCron: Boolean = CronExpression.isValidExpression(editScheduleJob.cronExpression)
            if (!flagCron) {
                return super.initErrorJSONObject(failResult,
                        "message.system.scheduleJob.cronExpression.error")
            }

            // 修改 ScheduleJob id
            val id: BigInteger? = scheduleJobService.updateBackId(editScheduleJob)
            if (null != id && BigInteger.ZERO < id) {
                if (StatusEnum.NORMAL.key == editScheduleJob.status) {
                    val scheduleJobNew: ScheduleJob? = scheduleJobService.getById(id)!!
                    // 异步执行定时任务
                    val scheduleJobOperationEvent = ScheduleJobOperationEvent(scheduleJobNew!!,
                            ScheduleJobOperationTypeEnum.UPDATE)
                    super.applicationContext.publishEvent(scheduleJobOperationEvent)
                }
            }
            return super.initUpdateJSONObject(id)
        } else {
            return super.initErrorRedirectUrl()
        }
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 查看页面
     *
     * @param id String
     * @param request HttpServletRequest
     * @param redirectAttributes RedirectAttributes
     * @return ModelAndView
     */
    @RequiresPermissions(value = [VIEW_URL])
    @RequestMapping("$VIEW/{id}")
    @RequestLogAnnotation(module = RequestLogConstant.ADMIN_MODULE,
            title = RequestLogConstant.ADMIN_MODULE_SYSTEM_SCHEDULE_JOB_VIEW)
    @Throws(ControllerException::class)
    fun view(@PathVariable(ID_NAME) id: String, request: HttpServletRequest,
             redirectAttributes: RedirectAttributes): ModelAndView {
        val modelAndView = ModelAndView(super.initView(VIEW_VIEW))
        super.initViewTitleAndModelUrl(VIEW_URL, MODULE_NAME, request)

        val idValue: BigInteger? = super.getId(id)
        // 验证数据
        if (null == idValue || BigInteger.ZERO > idValue) {
            return initValidationResult2Get(modelAndView, redirectAttributes, INDEX_URL)
        }
        // 数据真实性
        val scheduleJob: ScheduleJob? = scheduleJobService.getById(idValue)
                ?: return initValidationResult2Get(modelAndView, redirectAttributes, INDEX_URL)
        if (null != scheduleJob) {
            modelAndView.addObject(VIEW_MODEL_NAME, scheduleJob)
        }

        // 设置上个请求地址
        super.initRefererUrl(request)
        return modelAndView
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 删除
     * @param request HttpServletRequest
     * @param id String
     * @return JSONObject
     */
    @RequiresPermissions(value = [DELETE_URL])
    @RequestMapping("$DELETE/{id}")
    @ResponseBody
    @RequestLogAnnotation(module = RequestLogConstant.ADMIN_MODULE,
            title = RequestLogConstant.ADMIN_MODULE_SYSTEM_SCHEDULE_JOB_DELETE)
    @Throws(ControllerException::class)
    fun delete(@PathVariable(ID_NAME) id: String, request: HttpServletRequest): JSONObject? {
        if (super.isAjaxRequest(request)) {
            val idValue: BigInteger? = super.getId(id)
            // 验证数据
            if (null == idValue || BigInteger.ZERO > idValue) {
                super.initErrorCheckJSONObject()
            }
            val scheduleJob: ScheduleJob? = scheduleJobService.getById(idValue!!)
            if (null == scheduleJob) {
                super.initErrorCheckJSONObject()
            }

            // 删除 ScheduleJob id
            val backId: BigInteger? = scheduleJobService.removeByIdBackId(scheduleJob!!.id!!)
            if (null != backId && BigInteger.ZERO < backId) {
                // 异步执行定时任务
                val scheduleJobOperationEvent = ScheduleJobOperationEvent(scheduleJob,
                        ScheduleJobOperationTypeEnum.DELETE)
                super.applicationContext.publishEvent(scheduleJobOperationEvent)
            }
            return super.initDeleteJSONObject(backId)
        }
        return super.initReturnErrorJSONObject()
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 运行
     * @param request HttpServletRequest
     * @param id String
     * @return JSONObject
     */
    @RequiresPermissions(value = [RUN_URL])
    @RequestMapping("$RUN/{id}")
    @ResponseBody
    @RequestLogAnnotation(module = RequestLogConstant.ADMIN_MODULE,
            title = RequestLogConstant.ADMIN_MODULE_SYSTEM_SCHEDULE_JOB_RUN)
    @Throws(ControllerException::class)
    fun run(@PathVariable(ID_NAME) id: String, request: HttpServletRequest): JSONObject? {
        if (super.isAjaxRequest(request)) {
            val idValue: BigInteger? = super.getId(id)
            // 验证数据
            if (null == idValue || BigInteger.ZERO > idValue) {
                super.initErrorCheckJSONObject()
            }
            val scheduleJob: ScheduleJob? = scheduleJobService.getById(idValue!!)
            if (null == scheduleJob) {
                super.initErrorCheckJSONObject()
            }
            //  ScheduleJob id
            if (null != scheduleJob) {
                // 异步执行定时任务
                val scheduleJobOperationEvent = ScheduleJobOperationEvent(scheduleJob, ScheduleJobOperationTypeEnum.RUN)
                super.applicationContext.publishEvent(scheduleJobOperationEvent)
            }
            val resultData: ResultData = ResultData.ok()
            return super.initSucceedJSONObject(resultData, "message.run.succeed")
        }
        return super.initReturnErrorJSONObject()
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 暂停
     * @param request HttpServletRequest
     * @param id String
     * @return JSONObject
     */
    @RequiresPermissions(value = [PAUSE_URL])
    @RequestMapping("$PAUSE/{id}")
    @ResponseBody
    @RequestLogAnnotation(module = RequestLogConstant.ADMIN_MODULE,
            title = RequestLogConstant.ADMIN_MODULE_SYSTEM_SCHEDULE_JOB_PAUSE)
    @Throws(ControllerException::class)
    fun pause(@PathVariable(ID_NAME) id: String, request: HttpServletRequest): JSONObject? {
        if (super.isAjaxRequest(request)) {
            val idValue: BigInteger? = super.getId(id)
            // 验证数据
            if (null == idValue || BigInteger.ZERO > idValue) {
                super.initErrorCheckJSONObject()
            }
            val scheduleJob: ScheduleJob? = scheduleJobService.getById(idValue!!)
            if (null == scheduleJob) {
                super.initErrorCheckJSONObject()
            }
            //  ScheduleJob id
            if (null != scheduleJob) {
                // 异步执行定时任务
                val scheduleJobOperationEvent = ScheduleJobOperationEvent(scheduleJob, ScheduleJobOperationTypeEnum.PAUSE)
                super.applicationContext.publishEvent(scheduleJobOperationEvent)
            }
            val resultData: ResultData = ResultData.ok()
            return super.initSucceedJSONObject(resultData, "message.pause.succeed")
        }
        return super.initReturnErrorJSONObject()
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 恢复
     * @param request HttpServletRequest
     * @param id String
     * @return JSONObject
     */
    @RequiresPermissions(value = [RESUME_URL])
    @RequestMapping("$RESUME/{id}")
    @ResponseBody
    @RequestLogAnnotation(module = RequestLogConstant.ADMIN_MODULE,
            title = RequestLogConstant.ADMIN_MODULE_SYSTEM_SCHEDULE_JOB_RESUME)
    @Throws(ControllerException::class)
    fun resume(@PathVariable(ID_NAME) id: String, request: HttpServletRequest): JSONObject? {
        if (super.isAjaxRequest(request)) {
            val idValue: BigInteger? = super.getId(id)
            // 验证数据
            if (null == idValue || BigInteger.ZERO > idValue) {
                super.initErrorCheckJSONObject()
            }
            val scheduleJob: ScheduleJob? = scheduleJobService.getById(idValue!!)
            if (null == scheduleJob) {
                super.initErrorCheckJSONObject()
            }
            //  ScheduleJob id
            if (null != scheduleJob) {
                // 异步执行定时任务
                val scheduleJobOperationEvent = ScheduleJobOperationEvent(scheduleJob, ScheduleJobOperationTypeEnum.RESUME)
                super.applicationContext.publishEvent(scheduleJobOperationEvent)
            }
            val resultData: ResultData = ResultData.ok()
            return super.initSucceedJSONObject(resultData, "message.resume.succeed")
        }
        return super.initReturnErrorJSONObject()
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 验证数据 合法性
     *
     * @param request HttpServletRequest
     * @param scheduleJob         ScheduleJob
     * @return ScheduleJob
     */
    private fun initAddData(request: HttpServletRequest, scheduleJob: ScheduleJob): ScheduleJob {
        val addScheduleJob: ScheduleJob = ScheduleJob.copy2New(scheduleJob)
        // ip
        addScheduleJob.gmtCreateIp = super.getIp(request)
        // 创建 ScheduleJob 对象
        return scheduleJobService.initSaveScheduleJob(addScheduleJob)
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 验证数据 合法性
     *
     * @param scheduleJob         ScheduleJob
     * @return ScheduleJob
     */
    private fun initEditData(scheduleJob: ScheduleJob): ScheduleJob {
        val editScheduleJob: ScheduleJob = scheduleJob
        // 创建 ScheduleJob 对象
        return scheduleJobService.initEditScheduleJob(editScheduleJob)
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 验证数据 数据的唯一性
     *
     * @param scheduleJob         ScheduleJob
     * @param failResult ResultData
     * @return Boolean true(通过)/false(未通过)
     */
    private fun validationAddData(scheduleJob: ScheduleJob,
                                  failResult: ResultData):
            Boolean {
        val message: String
        // 是否存在
        val nameExist: ScheduleJob? = scheduleJobService.getByName(scheduleJob
                .name!!.trim().toLowerCase())
        if (null != nameExist) {
            message = localeMessageSourceConfiguration.getMessage(BaseAdminController.MESSAGE_NAME_EXIST_NAME)
            failResult.setFail()[configProperties.messageName] = message
            return false
        }
        val titleExist: ScheduleJob? = scheduleJobService.getByTitle(scheduleJob.title!!.trim().toLowerCase())
        if (null != titleExist) {
            message = localeMessageSourceConfiguration.getMessage(BaseAdminController.MESSAGE_TITLE_EXIST_NAME)
            failResult.setFail()[configProperties.messageName] = message
            return false
        }
        return true
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 验证数据 数据的唯一性
     *
     * @param scheduleJob         ScheduleJob
     * @param failResult ResultData
     * @return Boolean true(通过)/false(未通过)
     */
    private fun validationEditData(newScheduleJob: ScheduleJob, scheduleJob: ScheduleJob,
                                   failResult: ResultData): Boolean {
        val message: String

        val nameExist: Boolean = scheduleJobService.propertyUnique(ScheduleJob.NAME, newScheduleJob
                .name!!.trim().toLowerCase(), scheduleJob.name!!.toLowerCase())
        if (!nameExist) {
            message = localeMessageSourceConfiguration.getMessage(BaseAdminController.MESSAGE_NAME_EXIST_NAME)
            failResult.setFail()[configProperties.messageName] = message
            return false
        }

        val titleExist: Boolean = scheduleJobService.propertyUnique(ScheduleJob.TITLE, newScheduleJob
                .title!!.trim().toLowerCase(), scheduleJob.title!!.toLowerCase())
        if (!titleExist) {
            message = localeMessageSourceConfiguration.getMessage(BaseAdminController.MESSAGE_TITLE_EXIST_NAME)
            failResult.setFail()[configProperties.messageName] = message
            return false
        }

        return true
    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End ScheduleJobController class

/* End of file ScheduleJobController.kt */
/* Location: ./src/main/kotlin/wang/encoding/mroot/admin/controller/system/scheduleJob/ScheduleJobController.kt */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// | ErYang出品 属于小极品 O(∩_∩)O~~ 共同学习 共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
