/*
* // +-------------------------------------------------------------------------------------------------
* // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
* // +-------------------------------------------------------------------------------------------------
* // |                             独在异乡为异客         每逢佳节倍思亲
* // +-------------------------------------------------------------------------------------------------
* // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
* // +-------------------------------------------------------------------------------------------------
*/

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.admin.common.interceptor


import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import org.springframework.web.servlet.HandlerInterceptor
import org.springframework.web.servlet.LocaleResolver
import org.springframework.web.servlet.ModelAndView
import org.springframework.web.servlet.support.RequestContextUtils
import wang.encoding.mroot.admin.common.constant.ConfigConst
import wang.encoding.mroot.common.exception.BaseException
import wang.encoding.mroot.common.util.HttpRequestUtil
import java.util.*
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

/**
 * 默认拦截器
 *
 * @author ErYang
 */
@Component
class BaseInterceptor : HandlerInterceptor {


    @Autowired
    private lateinit var httpRequestUtil: HttpRequestUtil

    @Autowired
    private lateinit var configConst: ConfigConst

    companion object {

        /**
         * logger
         */
        private val logger: Logger = LoggerFactory.getLogger(BaseInterceptor::class.java)

        /**
         * .标识
         */
        private const val DOT_IDENTIFY = "."

    }

    // -------------------------------------------------------------------------------------------------


    @Throws(BaseException::class)
    override fun preHandle(request: HttpServletRequest, response: HttpServletResponse,
                           handler: Any): Boolean {

        response.characterEncoding = "UTF-8"

        val uri: String = request.requestURI
        // 静态请求直接跳出
        if (uri.contains(DOT_IDENTIFY)) {
            return true
        }
        if (httpRequestUtil.isAjaxRequest(request)) {
            return true
        }
        if (logger.isDebugEnabled) {
            logger.debug(">>>>>>>>BaseInterceptor>>>在请求[$uri]处理之前" +
                    "进行调用(Controller方法调用之前)<<<<<<<<")
        }
        // 只有返回true才会继续向下执行，返回false取消当前请求
        return true
    }

    // -------------------------------------------------------------------------------------------------

    @Throws(BaseException::class)
    override fun postHandle(request: HttpServletRequest, response: HttpServletResponse, handler: Any,
                            modelAndView: ModelAndView?) {
        // 国际化
        val localeResolverOptional: Optional<LocaleResolver> = Optional.ofNullable(RequestContextUtils.getLocaleResolver(request))
        if (localeResolverOptional.isPresent) {
            val localeResolver: LocaleResolver = localeResolverOptional.get()
            val i18n: String = localeResolver.resolveLocale(request).language
            if (logger.isDebugEnabled) {
                logger.debug(">>>>>>>>BaseInterceptor>>>[$i18n]<<<<<<<<")
            }
            request.setAttribute(configConst.i18nLanguageName, i18n)
        }
        val uri: String = request.requestURI
        // 静态请求直接跳出
        if (!uri.contains(DOT_IDENTIFY) && !httpRequestUtil.isAjaxRequest(request)) {
            if (logger.isDebugEnabled) {
                logger.debug(">>>>>>>>BaseInterceptor请求[$uri]处理之后进行调用," +
                        "但是在视图被渲染之前(Controller方法调用之后)调用<<<<<<<<")
            }
        }
    }

    // -------------------------------------------------------------------------------------------------

    @Throws(BaseException::class)
    override fun afterCompletion(request: HttpServletRequest, response: HttpServletResponse, handler: Any, ex: Exception?) {
        val uri: String = request.requestURI
        // 静态请求直接跳出
        if (!uri.contains(DOT_IDENTIFY) && !httpRequestUtil.isAjaxRequest(request)) {
            if (logger.isDebugEnabled) {
                logger.debug(">>>>>>>>BaseInterceptor>>>在整个请求[$uri]结束之后被调用,"
                        + "也就是在DispatcherServlet渲染了对应的视图之后执行(主要是用于进行资源清理工作)<<<<<<<<")
            }
        }
    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End BaseInterceptor class

/* End of file BaseInterceptor.kt */
/* Location: ./src/main/kotlin/wang/encoding/mroot/admin/common/interceptor/BaseInterceptor.kt */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
