/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.admin.common.util


import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import wang.encoding.mroot.admin.common.constant.ConfigConst
import wang.encoding.mroot.model.enums.*
import javax.servlet.ServletContext

/**
 * 页面使用的全局变量
 *
 * @author ErYang
 */
@Component
class SharedObjectUtil {


    @Autowired
    private lateinit var configConst: ConfigConst

    // -------------------------------------------------------------------------------------------------


    companion object {


        /**
         * 用户类型
         */
        private val USER_TYPE_MAP: MutableMap<Any, Any> = mutableMapOf()
        /**
         * 状态
         */
        private val STATUS_MAP: MutableMap<Any, Any> = mutableMapOf()
        /**
         * 权限类型
         */
        private val RULE_TYPE_MAP: MutableMap<Any, Any> = mutableMapOf()
        /**
         * 系统配置类型
         */
        private val CONFIG_TYPE_MAP: MutableMap<Any, Any> = mutableMapOf()

        /**
         * 是/否
         */
        private val BOOLE_MAP: MutableMap<Any, Any> = mutableMapOf()

        /**
         * 文章分类类型
         */
        private val CATEGORY_TYPE_MAP: MutableMap<Any, Any> = mutableMapOf()

        init {
            // 用户类型
            for (userTypeEnum: UserTypeEnum in UserTypeEnum.values()) {
                USER_TYPE_MAP[userTypeEnum.key.toString()] = userTypeEnum.value
            }

            // 状态
            for (statusEnum: StatusEnum in StatusEnum.values()) {
                STATUS_MAP[statusEnum.key.toString()] = statusEnum.value
            }

            // 权限类型
            for (ruleTypeEnum: RuleTypeEnum in RuleTypeEnum.values()) {
                RULE_TYPE_MAP[ruleTypeEnum.key.toString()] = ruleTypeEnum.value
            }

            // 系统配置类型
            for (configTypeEnum: ConfigTypeEnum in ConfigTypeEnum.values()) {
                CONFIG_TYPE_MAP[configTypeEnum.key.toString()] = configTypeEnum.value
            }

            // 是/否
            for (booleEnum: BooleEnum in BooleEnum.values()) {
                BOOLE_MAP[booleEnum.key.toString()] = booleEnum.value
            }

            // 文章分类类型
            for (categoryTypeEnum: CategoryTypeEnum in CategoryTypeEnum.values()) {
                CATEGORY_TYPE_MAP[categoryTypeEnum.key.toString()] = categoryTypeEnum.value
            }

        }
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 放入 ServletContext 里面
     *
     * @param servletContext ServletContext
     */
    fun init(servletContext: ServletContext) {
        // 用户类型
        servletContext.setAttribute(configConst.userTypeMap, USER_TYPE_MAP)
        // 状态
        servletContext.setAttribute(configConst.statusMap, STATUS_MAP)
        servletContext.setAttribute(configConst.statusNormal, StatusEnum.NORMAL.key)
        servletContext.setAttribute(configConst.statusDisable, StatusEnum.DISABLE.key)
        servletContext.setAttribute(configConst.statusDelete, StatusEnum.DELETE.key)

        // 权限类型
        servletContext.setAttribute(configConst.ruleTypeMap, RULE_TYPE_MAP)
        //servletContext.setAttribute(configConst.ruleTypeRoot, RuleTypeEnum.ROOT.key)
        servletContext.setAttribute(configConst.ruleTypeUrl, RuleTypeEnum.URL.key)
        servletContext.setAttribute(configConst.ruleTypeMenu, RuleTypeEnum.MENU.key)
        servletContext.setAttribute(configConst.ruleTypeChildMenu, RuleTypeEnum.CHILD_MENU.key)
        servletContext.setAttribute(configConst.ruleTypeButton, RuleTypeEnum.BUTTON.key)

        // 系统配置类型
        servletContext.setAttribute(configConst.configTypeMap, CONFIG_TYPE_MAP)
        servletContext.setAttribute(configConst.configTypeValue, ConfigTypeEnum.VALUE.key)
        servletContext.setAttribute(configConst.configTypeFun, ConfigTypeEnum.FUN.key)

        // 是/否
        servletContext.setAttribute(configConst.booleMap, BOOLE_MAP)
        servletContext.setAttribute(configConst.booleYes, BooleEnum.YES.key)
        servletContext.setAttribute(configConst.booleNo, BooleEnum.NO.key)


        // 文章分类类型
        servletContext.setAttribute(configConst.categoryTypeMap, CATEGORY_TYPE_MAP)
        servletContext.setAttribute(configConst.categoryTypeFirst, CategoryTypeEnum.FIRST.key)
        servletContext.setAttribute(configConst.categoryTypeSecond, CategoryTypeEnum.SECOND.key)
        servletContext.setAttribute(configConst.categoryTypeThird, CategoryTypeEnum.THIRD.key)

    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End SharedObjectUtil class

/* End of file SharedObjectUtil.kt */
/* Location: ./src/main/kotlin/wang/encoding/mroot/admin/common/util/SharedObjectUtil.kt */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
