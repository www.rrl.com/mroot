/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.admin.common.shiro


import org.apache.shiro.authc.AuthenticationInfo
import org.apache.shiro.authc.AuthenticationToken
import org.apache.shiro.authc.UsernamePasswordToken
import org.apache.shiro.authc.credential.SimpleCredentialsMatcher
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import wang.encoding.mroot.common.config.DigestManageConfiguration

/**
 * 自定义密码加密方式
 *
 * @author ErYang
 */
@Component
class MyCredentialsMatcher : SimpleCredentialsMatcher() {

    @Autowired
    private lateinit var digestManageConfiguration: DigestManageConfiguration

    /**
     * 密码加密  subject.login(usernamePasswordToken) 会调用
     *
     * @param token 令牌
     * @param info  信息
     * @return true/false
     */
    override fun doCredentialsMatch(token: AuthenticationToken, info: AuthenticationInfo): Boolean {

        // 用户 usernamePasswordToken 信息
        val usernamePasswordToken: UsernamePasswordToken = token as UsernamePasswordToken

        // 用户名和密码进行加密
        val tokenCredentials: String = digestManageConfiguration.getSha512(String(usernamePasswordToken.password))

        val accountCredentials: Any = getCredentials(info)
        // 将密码加密与系统加密后的密码校验 内容一致就返回 true 不一致就返回 false
        return super.equals(tokenCredentials, accountCredentials)
    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End MyCredentialsMatcher class

/* End of file MyCredentialsMatcher.kt */
/* Location: ./src/main/kotlin/wang/encoding/mroot/admin/common/shiro/MyCredentialsMatcher.kt */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
